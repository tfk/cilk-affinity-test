// Copyright 2014

#include <assert.h>
#include <cilk/cilk.h>
#include <cilk/common.h>
#include <sys/resource.h>
#include <sys/time.h>

#include <algorithm>
#include <cmath>
#include <ctime>

#include <string>
#include <vector>

#include "./tfk_utils.h"

// Static variables for convenience.
// Most of these aren't needed, but are good for sanity checking.
static int total_updates = 0;
static int* starts;
static int* ends;
static int* success_vector;

void do_loop_section(int N, double* array, double* array1, int wid) {
  success_vector[wid] = 1;
  int num_workers = __cilkrts_get_nworkers();
  int start = starts[wid];
  int end = ends[wid];
  cilk_for (int i = start; i < end; i++) {
    double sum = array[i];
    if (i > 0) sum += array[i-1];
    if (i < N) sum += array[i+1];
    array1[i] = sum/3;
    // NOTE(TFK): Uncomment to make sure the total_updates is correct.
    // __sync_fetch_and_add(&total_updates, 1);
  }
}

// New version that has O(1) contention.
void steal_loop(int N, double* array, double* array1, int* work_list,
    int steal_wid) {
  // Need to check that I can't do my worker_id.
  int wid = __cilkrts_get_worker_number();
  if (__sync_bool_compare_and_swap(&(work_list[wid]), 0, 1)) {
    // Perform work wid did last iteration.
    cilk_spawn do_loop_section(N, array, array1, wid);
    steal_loop(N, array, array1, work_list, steal_wid);
  } else {
    // Do the work of the other worker, if not already done.
    if (__sync_bool_compare_and_swap(&(work_list[steal_wid]), 0, 1)) {
      do_loop_section(N, array, array1, steal_wid);
    }
  }
}

int main(int argc, char **argv) {
  int N = (1<<20);
  int REPS = 50;
  bool VERIFY = false;
  if (argc > 2) {
    printf("N=%s REPS=%s \n", argv[1], argv[2]);
    N = StringToNumber<int>(std::string(argv[1]));
    REPS = StringToNumber<int>(std::string(argv[2]));
    if (argc > 3) {
      printf("Running affinity loop.\n");
      VERIFY = true;
    } else {
      printf("Running regular cilk_for loop\n");
    }
  } else {
    printf("Usage: ./main <N> <REPS> <?> ---"
        "include optional 3rd argument to run affinity loop\n");
    return 0;
  }

  // Perform static partitioning among the workers.
  starts =
      reinterpret_cast<int*>(calloc(__cilkrts_get_nworkers(), sizeof(int)));
  ends =
      reinterpret_cast<int*>(calloc(__cilkrts_get_nworkers(), sizeof(int)));
  int block_size = N/__cilkrts_get_nworkers();
  starts[0] = 0;
  ends[0] = block_size;
  for (int i = 1; i < __cilkrts_get_nworkers(); i++) {
    starts[i] = starts[i-1] + block_size;
    ends[i] = starts[i] + block_size;
  }
  ends[__cilkrts_get_nworkers()-1] = N;
  for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
    printf("starts %d, ends %d\n", starts[i], ends[i]);
  }

  double* _array1 = reinterpret_cast<double*>(calloc(N, sizeof(double)));
  double* _array2 = reinterpret_cast<double*>(calloc(N, sizeof(double)));
  for (int i = 0; i < N; i++) {
    _array1[i] = i;
    _array2[i] = i;
  }

  double total_collect_time = 0;
  double start_time, end_time;

  // NOTE(TFK): Skip the serial work to avoid possible cache effects.
  // start_time = tfk_get_time();
  /*
  for (int a = 0; a < REPS; a++) {
    int* array = a%2==0 ? array1 : array2;
    int* array1 = a%2==1 ? array1 : array2;
    for (int i = 0; i < N; i++) {
      int sum = array[i];
      if (i > 0) sum += array[i-1];
      if (i < N) sum += array[i+1];
      array1[i] = sum/3;
    }
  }*/
  // end_time = tfk_get_time();

  // printf("serial stencil time is %f\n", end_time - start_time);
  // double serial_runtime = end_time-start_time;
  start_time = tfk_get_time();
  int total_items = 0;

  for (int a = 0; a < REPS; a++) {
    double* array = (a%2) == 0 ? _array1 : _array2;
    double* array1 = (a%2) == 0 ? _array2 : _array1;

    if (VERIFY) {
      int* work_array = reinterpret_cast<int*>(
          calloc(__cilkrts_get_nworkers(), sizeof(int)));
      success_vector = reinterpret_cast<int*>(
          calloc(__cilkrts_get_nworkers(), sizeof(int)));
      #pragma cilk grainsize = 1
      cilk_for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
        steal_loop(N, array, array1, work_array, i);
      }

      // NOTE(TFK): Old version that has P contention
      // cilk_spawn do_loop(N, array, array1, work_array);
      //       do_loop(N, array, array1, work_array);
      // cilk_sync;
      // NOTE(TFK): Uncomment to verify that all work was performed.
      // for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
      //   assert(success_vector[i] == 1);
      // }
      free(work_array);
      free(success_vector);
    } else {
      cilk_for (int i = 0; i < N; i++) {
        double sum = array[i];
        if (i > 0) sum += array[i-1];
        if (i < N) sum += array[i+1];
        array1[i] = sum/3;
      }
    }
  }
  end_time = tfk_get_time();
  printf("total number of updates %d\n", total_updates);
  printf("total parallel stencil time is %f\n", end_time-start_time);
  return 0;
}

// Old version which suffers from P contention.
/*
void do_loop(int N, double* array, double* array1, int* work_list) {
  int wid = __cilkrts_get_worker_number();
  if (__sync_bool_compare_and_swap(&(work_list[wid]), 0, 1)) {
    // Perform work wid did last iteration.
    cilk_spawn do_loop_section(N,array, array1, wid);
  } else {
    // Steal another worker's work.
    bool found_work = false;
    int work_id = -1;
    for (int i = 0; i < __cilkrts_get_nworkers(); i++) {
      // P contention.
      if (__sync_bool_compare_and_swap(&(work_list[i]),0,1)) {
         found_work = true;
         work_id = i;
         break;
      }
    }
    if (found_work) {
      cilk_spawn do_loop_section(N,array, array1, work_id);
    } else {
      // All work is done, time to end the recursion.
      return;
    }
  }
  // Recurse
  do_loop(N,array, array1, work_list);
}
*/


