CC=icc
CFLAGS= -g
CILK=icpc
CILKFLAGS= -w -O3 -g -fcilkplus -lcilkrts -L/usr/local/lib
LDFLAGS= -L$(CURDIR)
#AR=ar

all: main

main : main.cpp tfk_utils.h Makefile
	$(CILK) $(CILKFLAGS) $@.cpp $(LDFLAGS) -o $@

clean :
	rm -f main *~

run :
	./run.sh $(file) $(type) $(nworkers)
